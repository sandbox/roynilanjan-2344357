<?php

/**
 * @file
 * This file consists the configuration for media file usage.
 */

/**
 * This is the callback to use form API to built an admin interface.
 *
 * @see https://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7.
 */
function media_file_usage_settings_entity($form, &$form_state) {

  //dsm(entity_get_info('node'));
  $form['#tree'] = TRUE;
  $form['media_file_usage_entity_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entity'),
    '#prefix' => '<div id="media-usage-entity-wrapper">',
    '#suffix' => '</div>',
  );

  if (empty($form_state['counter'])) {
    $form_state['counter'] = 1;
  }

  for ($i = 1; $i <= $form_state['counter']; $i++) {
    $form_state['var'] = $i;
    if (!empty($form_state['values'])) {
      $selected = $form_state['values']['media_file_usage_entity_fieldset']['node'][$i]['dropdown_first'][$i];
      $options = media_file_usage__field_options($selected);
    }
    $form['media_file_usage_entity_fieldset']['node'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t('Node'),
      '#collapsible' => TRUE, 
      '#collapsed' => TRUE,
    );    
    $form['media_file_usage_entity_fieldset']['node'][$i]['dropdown_first'][$i] = array(
      '#type' => 'select',
      '#title' => t('Bundle'),
      '#options' => array('a' => 'article', 'b' => 'page'),
      '#ajax' => array(
      'callback' => 'media_file_usage_dropdown_callback',
      'wrapper' => "dropdown-second-replace-{$i}",
      ),
    );
    $form['media_file_usage_entity_fieldset']['node'][$i]['dropdown_second'][$i] = array(
      '#type' => 'select',
      '#title' => '',
    
      '#prefix' => "<div id=\"dropdown-second-replace-{$i}\">",
      '#suffix' => '</div>',
    
      '#options' => !empty($options) ? $options : '',
      '#default_value' => '',
    );
  }
  $form['media_file_usage_entity_fieldset']['add_name'] = array(
    '#type' => 'submit',
    '#value' => t('Add one more'),
    '#submit' => array('media_file_usage_submit'),
    '#ajax' => array(
      'callback' => 'media_usage_add_more_callback',
      'wrapper' => 'media-usage-entity-wrapper',
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}


function media_file_usage_dropdown_callback($form, $form_state, $counter) {
  $var = $form_state['var']; 
  return $form['media_file_usage_entity_fieldset']['node'][$var]['dropdown_second'][$var];  
}


function media_file_usage_submit($form, &$form_state) {
  $form_state['counter']++;
  $form_state['rebuild'] = TRUE;
}

function media_usage_add_more_callback($form, $form_state) {
  return $form['media_file_usage_entity_fieldset'];
}

function media_file_usage_settings_entity_submit($form, $form_state) {
  dpm($form_state);
}

function media_file_usage__field_options($key) {
  $options = array(
    'a' => array('X', 'Y', 'Z'),
    'b' => array('A', 'B', 'C'),
  );
  return $options[$key];
}
